<?php
  class ClearPar {
    public $textoIN;
    public $textoOUT = "";

    public function __construct($str) {
        $this->textoIN = $str;
    }
        
    public function build() {
       $arrayIN = str_split($this->textoIN, 1);
       $total = count($arrayIN);
       for($i=0; $i < $total; $i++) {
            $value = $arrayIN[$i];
           if($arrayIN[$i] == '(') {
              for ($j=$i; $j < $total; $j++) { 
                 if($arrayIN[$j] == ')'){
                   $this->textoOUT = $this->textoOUT.$arrayIN[$i].$arrayIN[$j];
                   $i = $j;
                   break;
                 }
              }
           }
       }
    }
    public function getTextoOUT(){
      return $this->textoOUT;
    }
    
}

?>