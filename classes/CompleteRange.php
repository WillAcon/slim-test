<?php
  class CompleteRange {
    public $arrayIN;
    public $arrayOUT;

    public function __construct($array) {
        $this->arrayIN = $array;
    }
        
    public function build() {
  	    $total_in = count( $this->arrayIN );
  	    $initValue = $this->arrayIN[0];
  	    $endValue = $this->arrayIN[ $total_in - 1 ];

  	    $total = (int)$endValue - (int)$initValue;

  	    $this->arrayOUT[0] = $initValue;

  	   for( $i=1; $i <= $total; $i++ ) {
  	   	 $this->arrayOUT[$i] = $this->arrayOUT[$i - 1] + 1;
  		 }

    }
    public function getArrayOUT(){
    	return $this->arrayOUT;
    }

    public function PrintArray($array) {
        echo "[ ";
        for($i=0; $i < count($array); $i++) {
	     	  echo $array[$i]." ";
		    }
        echo "]";
    }
}

?>