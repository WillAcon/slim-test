<?php
  class ChangeString {
    public $textoIN;
    public $textoOUT = "";
    
    public function __construct($str) {
        $this->textoIN = $str;
    }
        
    public function build() {
        $arrayIN = $this->str_split_unicode($this->textoIN, 1);
        $total = count($arrayIN);

       for($i=0; $i < $total; $i++) {
            $value = $arrayIN[$i];
           if(preg_match('/[ñnz]/i',$value)) {
                $value = $this->other_letter($value);
           }
           else {
                if($this->is_letter($value)) {
                    $value = $this->nex_letter( $value );                  
                 }
           }
           $this->textoOUT = $this->textoOUT.$value;    
       }
    }
    public function getTextoOUT(){
        return $this->textoOUT;
    }
    
    public function is_letter($letter) {
        if( preg_match('/[a-zs-]/i', $letter) ) {
          return true;
        }
        else {
          return false;
        }
    }
    /* letters extras */
    function other_letter($letter){
        switch ($letter) {
            case 'Ñ':
                return "O";
            case 'ñ':
                return "o";
            case 'Z':
                return "A";
            case 'z':
                return "a";
            case 'n':
                return "ñ";
            default:
                return "Ñ";
         }
    }
    /* get next letter */
    function nex_letter($letter) {
        $letraIn = ord($letter);
        $valueNext = chr($letraIn + 1);
        return $valueNext;
    }
    /*Get str to array*/
    function str_split_unicode($str, $l = 0) {
        if ($l > 0) {
            $ret = array();
            $len = mb_strlen($str, "UTF-8");
            for ($i = 0; $i < $len; $i += $l) {
                $ret[] = mb_substr($str, $i, $l, "UTF-8");
            }
            return $ret;
        }
        return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    }
    
}

?>