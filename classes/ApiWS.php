<?php

class ApiWS extends Mapper{
    //get XML Employee Table
    public function getApi($min = '', $max= '') {
        $result = $this->db;
        if($min != '' && $max != '') {
            $result = $this->getListEmployee($min, $max);    
        }
        $xml = new SimpleXMLElement('<?xml version="1.0"?><Company></Company>');
        foreach($result as $key=>$value) {
          $Employee = $xml->addChild('Employee');
          foreach($result[$key] as $key1=>$value1) {
            if(is_array($value1)) {
              $Item = $Employee->addChild($key1);
              foreach ($value1 as $key2 => $value2) {
                $Item->addChild(key($value2), $value2['skill']);  
              }
            }else{
              $Employee->addChild($key1, $value1);  
            }
          }
        }
        return $xml->asXML();
    }
    function getListEmployee($min, $max) {
        $j = 0;
        $result = array();

        foreach($this->db as $employe) {          
            $min_value = str_replace("$","",$employe['salary']);
            $min_value = floatval(str_replace(",","",$min_value));
            if ($min_value >= $min && $min_value <= $max) {
                $result[$j] = $employe;
                $j++;
            }
        }

        return $result;
    }
  
}
