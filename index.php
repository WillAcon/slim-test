<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

spl_autoload_register(function ($classname) {
    require ( "classes/".$classname . ".php");
});

date_default_timezone_set('America/Lima');

// Create app
$app = new \Slim\App();

// Get container
$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = json_decode(file_get_contents("data/employees.json"), true);
    return $db;
};

// Register twig component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('templates');
    
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->addExtension(new Twig_Extension_Debug());
    return $view;
};

$app->get("/", function (Request $request, Response $response, $args) {
	$html = "<a href='/one'>Uno</a> | ";
	$html .= "<a href='/two'>Dos</a> | ";
	$html .= "<a href='/three'>Tres</a> | ";
	$html .= "<a href='/four/employees/list'>Cuatro</a>";

	echo $html;

});


$app->get("/one", function() {
	  $Change = new ChangeString("KjsjdhfhUuuYDJDJDUUggXXZZz778232lññ YR ''OyGGy");
		  echo "DATA IN<br>";
		  echo $Change->textoIN;
		  echo "<br>";
	  $Change->build();
		  echo "DATA OUT<br>";
	      echo $Change->textoOUT;
	      echo "<br><a href='/'>Volver</a>";
	      echo "<hr>";
	      show_source("classes/ChangeString.php");
});

$app->get("/two", function() {
  $arrayIN = array(16,19,25,35);
  $Complete = new CompleteRange($arrayIN);
  	 echo "DATA IN<br>";
  $Complete->PrintArray($Complete->arrayIN);
  $Complete->build();
 	 echo "<br>DATA OUT<br>";
  $Complete->PrintArray($Complete->arrayOUT);
  	 echo "<br><a href='/'>Volver</a>";
  	 echo "<hr>";
	 show_source("classes/CompleteRange.php");
});

$app->get("/three", function() {
  $ArrayIn = array(0=>"()(((()(((()()", 1=>")(", 2=>"))))(()", 3=>"((()", 4=>"(((((((((()()");
  $StringIn = $ArrayIn[rand(0,4)];
  $Clear = new ClearPar($StringIn);
	  echo "DATA IN<br>";
	  echo $Clear->textoIN;
	  echo "<br>DATA OUT<br>";
  $Clear->build();
	  echo $Clear->textoOUT;
	  echo "<br><a href='/'>Volver</a>";
	  echo "<hr>";
	  show_source("classes/ClearPar.php");

});



// API group
$app->group('/four', function () use ($app) {
    // Version group
	 $app->group('/employees', function () use ($app) {
		 $app->get('/list', function (Request $request, Response $response, $args) {
		 	$email = '';
		 	if($request->getParam('email')) {
		 		$email = $request->getParam('email');
		 	}
		 	$Employees = new EmployeesMapper($this->db);
		 	$listEmployees = $Employees->getEmployees($email);
			    $response = $this->view->render($response, "employees.twig", [
			        'employees' => $listEmployees,
			        'search' => $email
			    ]);
			    return $response;
		 });
		 $app->get('/view/{id}', function (Request $request, Response $response, $args) {
		 	$id = $args['id'];
		 	$Employee = new EmployeesMapper($this->db);
		 	$ViewEmploye = $Employee-> getPropietarioByID($id);
			    $response = $this->view->render($response, "employee_profile.twig", [
			        'employee' => $ViewEmploye
			    ]);
			    return $response;
		 });
	 });
});

$app->group('/api', function () use ($app) {
	 $app->group('/v1', function () use ($app) {
		 $app->get('/employee/{min}[/{max}]', function (Request $request, Response $response, $args) {
		 	$body = $response->getBody();
		 	$Employees = new ApiWS($this->db);
		 	$XMLEmployees = $Employees->getApi($args['min'], $args['max']);
		 	$body->write($XMLEmployees);
		 	 return $response->withHeader(
		        'Content-Type',
		        'text/xml'
		    )->withBody($body);
		 });
		 $app->get("/home", function (Request $request, Response $response, $args) {
		    $response = $this->view->render($response, "home.twig");
		    return $response;
		});

	 });

});


$app->run();

